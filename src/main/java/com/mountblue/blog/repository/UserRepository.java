package com.mountblue.blog.repository;

import com.mountblue.blog.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface UserRepository extends JpaRepository<User, Integer> {
    public User findByNameAndEmail(String name, String email);

    @Query("from User where name like :s")
    List<User> findAllUserWithThisWordInName(String s);

    @Query("select distinct name from User")
    List<String> getAllUserName();

    User findByEmail(String username);

    List<User> findByName(String name);
}
