package com.mountblue.blog.repository;

import com.mountblue.blog.entity.Tag;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface TagRepository extends JpaRepository<Tag, Integer> {


    @Query("select name from Tag where name like ?1")
    List<String> searchMatchingTag(String tag);

    Tag findDistinctByName(String name);

    List<Tag> findByName(String name);

    @Query("from Tag where name like ?1")
    List<Tag> getAllTagWithName(String name);

    @Query("select name from Tag")
    List<String> getAllTagNames();
}
