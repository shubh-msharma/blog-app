package com.mountblue.blog.repository;

import com.mountblue.blog.entity.Post;
import com.mountblue.blog.entity.PostTag;
import com.mountblue.blog.entity.Tag;
import com.mountblue.blog.entity.compositKey.postTagId;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface PostTagRepository extends JpaRepository<PostTag, postTagId> {
    List<PostTag> findAllByPosts(Post post);

    int deleteByPosts(Post post);

    void deleteByPostsAndTags(Post posts, Tag tags);
}
