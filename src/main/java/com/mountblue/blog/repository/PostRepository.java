package com.mountblue.blog.repository;

import com.mountblue.blog.entity.Post;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

@Repository
public interface PostRepository extends JpaRepository<Post, Integer> {
    @Query("from Post where title like :s")
    List<Post> fetchAllPostByTitle(String s);

    @Query("from Post p where p.content like :s")
    List<Post> fetchAllPostByContent(String s);

    @Query("select p from Post p where DATE(p.publishedAt) = DATE(:s)")
    List<Post> fetchAllPostByDate(Date s);


}
