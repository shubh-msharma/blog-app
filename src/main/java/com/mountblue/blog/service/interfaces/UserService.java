package com.mountblue.blog.service.interfaces;


import com.mountblue.blog.entity.Comment;
import com.mountblue.blog.entity.Post;
import com.mountblue.blog.entity.User;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface UserService extends UserDetailsService {
    User getTransientUserObject(String name, String email);

    User saveUser(User user);

    User findUserByNameAndEmail(String name, String mail);

    void removeCommentsFromUser(User user, List<Comment> commentList);

    List<User> findAllUserWithName(String search);

    List<String> getAllUserNames();

    User findUserByEmail(String name);

    Boolean isAllowedToModify(Post post);

    Boolean isAdmin();

    List<User> findByName(String name);

    List<Post> findPostByUserNames(List<String> users);
}
