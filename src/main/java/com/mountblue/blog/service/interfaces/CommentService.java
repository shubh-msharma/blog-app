package com.mountblue.blog.service.interfaces;

import com.mountblue.blog.entity.Comment;
import com.mountblue.blog.entity.Post;
import com.mountblue.blog.entity.User;

import java.util.List;

public interface CommentService {
    Comment getTransientCommentObject(Post post, User user, String commentText);

    Comment saveComment(Comment comment);

    Comment findCommentByUserAndPost(User user, Post post);

    void deleteComment(Comment comment);

    void deleteAllComments(List<Comment> commentList);

    Comment findById(int commentId);
}
