package com.mountblue.blog.service.interfaces;

import com.mountblue.blog.entity.Role;

public interface RoleService {
    Role saveRole(Role role);

    Role findRoleByRole(String role);
}

