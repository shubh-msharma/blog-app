package com.mountblue.blog.service.interfaces;


import com.mountblue.blog.entity.Post;
import com.mountblue.blog.entity.PostTag;
import com.mountblue.blog.entity.Tag;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface PostTagService {

    List<PostTag> fetchPostTagsByTagAndPost(List<Tag> tagList, Post post);

    List<PostTag> saveAllPostTags(List<PostTag> postTagsList);

    List[] fetchPostTagAndTagByPostAndTag(List<Tag> tagList, Post post);

    void deleteAll(List<PostTag> postTagsToBeRemoved);

    List<PostTag> removePostRelatedPostTagAndTag(Post post);
}
