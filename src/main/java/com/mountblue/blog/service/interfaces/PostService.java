package com.mountblue.blog.service.interfaces;


import com.mountblue.blog.entity.Post;
import com.mountblue.blog.entity.User;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

import java.text.ParseException;
import java.util.Collection;
import java.util.Date;
import java.util.List;

@Service
public interface PostService {

    List<Post> getAllPost();

    Post findById(int id);

    Post getTransientPostObject(String content, String title);

    Page<Post> getPaginatedPost(int page, String sortingColumn);

    Post savePost(Post post);

    void deletePost(Post post);

    List<Post> fetchAllPostByAuther(List<User> userList);

    List<Post> fetchAllPostByTitle(String search);

    List<Post> fetchAllPostByContent(String search);

    List<Post> fetchAllPostByDate(Date date);

    Date getCurrentDate() throws ParseException;

    Date getCurrentDate(String date) throws ParseException;

}
