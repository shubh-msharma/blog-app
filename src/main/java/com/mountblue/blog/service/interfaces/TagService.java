package com.mountblue.blog.service.interfaces;


import com.mountblue.blog.entity.Post;
import com.mountblue.blog.entity.PostTag;
import com.mountblue.blog.entity.Tag;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface TagService {

    List<Tag> fetchTagsByTagNames(List<String> tagNames);

    List<Tag> saveAllTags(List<Tag> tagList);

    List<Tag> removePostTags(List<PostTag> postTags);

    List<Post> fetchAllPostByTag(String search);

    List<String> getAllTagNames();

    List<Post> findPostByTagNames(List<String> tags);
}
