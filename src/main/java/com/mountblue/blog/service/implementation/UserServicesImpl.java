package com.mountblue.blog.service.implementation;

import com.mountblue.blog.entity.Comment;
import com.mountblue.blog.entity.Post;
import com.mountblue.blog.entity.Role;
import com.mountblue.blog.entity.User;
import com.mountblue.blog.repository.UserRepository;
import com.mountblue.blog.service.interfaces.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class UserServicesImpl implements UserService {
    @Autowired
    private UserRepository userRepo;

    @Override
    public User getTransientUserObject(String name, String email) {
        User user = new User();
        user.setName(name);
        user.setEmail(email);
        return user;
    }

    @Override
    public User saveUser(User user) {
        return userRepo.save(user);
    }

    @Override
    public User findUserByNameAndEmail(String name, String mail) {
        User user = userRepo.findByNameAndEmail(name, mail);
        return user;
    }

    @Override
    public void removeCommentsFromUser(User user, List<Comment> commentList) {
        for (Comment comment : commentList) {
            user.removeComment(comment);
        }
    }

    @Override
    public List<User> findAllUserWithName(String search) {
        return userRepo.findAllUserWithThisWordInName("%" + search + "%");
    }

    @Override
    public List<String> getAllUserNames() {
        return userRepo.getAllUserName();
    }

    @Override
    public User findUserByEmail(String email) {
        return userRepo.findByEmail(email);
    }

    @Override
    public Boolean isAllowedToModify(Post post) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        if (auth.getName() != "anonymousUser") {
            if (userRepo.findByEmail(auth.getName()).getRoles().stream().anyMatch(role -> role.getRole().equals("admin")) || auth.getName().equals(post.getAuthor().getEmail())) {
                return true;
            }
        }
        return false;
    }

    @Override
    public Boolean isAdmin() {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        if (auth.getName() != "anonymousUser") {
            User user = userRepo.findByEmail(auth.getName());
            if (user != null) {
                return user.getRoles().stream().anyMatch(role -> role.getRole().equals("admin"));
            }
        }
        return false;
    }

    @Override
    public List<User> findByName(String name) {
        return userRepo.findByName(name);
    }

    @Override
    public List<Post> findPostByUserNames(List<String> users) {
        List<Post> postList = new ArrayList<Post>();
        for (String name : users) {
            List<User> userList = userRepo.findByName(name);
            if (userList != null) {
                for (User user : userList) {
                    postList.addAll(user.getPosts());
                }
            }
        }
        return postList;
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        User user = userRepo.findByEmail(username);
        if (user == null) {
            throw new UsernameNotFoundException("Incorrect User name or Password");
        }
        return new org.springframework.security.core.userdetails.User(user.getEmail(), user.getPassword(), getGrantedAuthoriteis(user.getRoles()));
    }

    private Collection<? extends GrantedAuthority> getGrantedAuthoriteis(Collection<Role> roles) {
        return roles.stream().map(role -> new SimpleGrantedAuthority(role.getRole())).collect(Collectors.toList());
    }
}
