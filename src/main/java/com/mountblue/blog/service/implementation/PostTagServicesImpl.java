package com.mountblue.blog.service.implementation;


import com.mountblue.blog.entity.Post;
import com.mountblue.blog.entity.PostTag;
import com.mountblue.blog.entity.Tag;
import com.mountblue.blog.entity.compositKey.postTagId;
import com.mountblue.blog.repository.PostTagRepository;
import com.mountblue.blog.service.interfaces.PostTagService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Transactional
@Service
public class PostTagServicesImpl implements PostTagService {
    @Autowired
    private PostTagRepository postTagRepository;

    public static List<Tag> getTagListFromPostTagList(List<PostTag> postTagList) {

        List<Tag> tagList = new ArrayList<Tag>();
        for (PostTag postTag : postTagList) {
            tagList.add(postTag.getTags());
        }
        return tagList;
    }

    public PostTag addPostTag(Post post, Tag tag) {
        PostTag postTag = new PostTag();
        postTagId postTagId = new postTagId();
        postTagId.setPostId(post.getId());
        postTagId.setTagId(tag.getId());
        postTag.setId(postTagId);
        postTag.setCreatedAt(new Date());
        postTag.setPosts(post);
        postTag.setTags(tag);
        return postTagRepository.save(postTag);
    }

    @Override
    public List<PostTag> fetchPostTagsByTagAndPost(List<Tag> tagList, Post post) {
        List<PostTag> posttagList = new ArrayList<PostTag>();
        List<Tag> existingTagList = getTagListFromPostTagList(post.getTags());
        for (Tag tag : tagList) {
            if (!existingTagList.contains(tag)) {
                PostTag postTag = new PostTag();
                postTag.setId(new postTagId(post.getId(), tag.getId()));
                postTag.setTags(tag);
                postTag.setPosts(post);
                tag.setPosts(postTag);
                post.setTags(postTag);
                posttagList.add(postTag);
                System.out.println("new tag added to post");
            }
        }
        return posttagList;
    }

    @Override
    public List<PostTag> saveAllPostTags(List<PostTag> postTagsList) {
        return postTagRepository.saveAll(postTagsList);
    }

    @Override
    public List[] fetchPostTagAndTagByPostAndTag(List<Tag> tagList, Post post) {
        List<PostTag> oldPostTagList = post.getTags();
        List<PostTag> postTagsToBeRemoved = new ArrayList<PostTag>();
        List<Tag> tagToBeUpdated = new ArrayList<Tag>();
        for (PostTag postTag : oldPostTagList) {
            if (!tagList.contains(postTag.getTags())) {
                postTagsToBeRemoved.add(postTag);
                post.removePostTag(postTag);
                postTag.getTags().removePostTag(postTag);
                tagToBeUpdated.add(postTag.getTags());
            }
        }
        return new List[]{postTagsToBeRemoved, tagToBeUpdated};
    }

    @Override
    public void deleteAll(List<PostTag> postTagsToBeRemoved) {
        postTagRepository.deleteAll(postTagsToBeRemoved);
    }

    @Override
    public List<PostTag> removePostRelatedPostTagAndTag(Post post) {
        List<PostTag> postTags = post.getTags();
        return postTags;
    }
}
