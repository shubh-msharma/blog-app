package com.mountblue.blog.service.implementation;

import com.mountblue.blog.entity.Post;
import com.mountblue.blog.entity.PostTag;
import com.mountblue.blog.entity.Tag;
import com.mountblue.blog.repository.TagRepository;
import com.mountblue.blog.service.interfaces.TagService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
public class TagServicesImpl implements TagService {
    @Autowired
    TagRepository tagRepository;

    public Tag saveTag(String name) {
        List<Tag> existingTag = tagRepository.findByName(name);
        if (existingTag == null || existingTag.isEmpty()) {
            Tag tag = new Tag();
            tag.setName(name);
            tag.setCreatedAt(new Date());
            return tagRepository.save(tag);
        } else {
            return existingTag.get(0);
        }
    }

    public List<String> searchMatchingTag(List<String> tagList) {
        List<String> newTagList = new ArrayList<String>();
        for (String tag : tagList) {
            newTagList.addAll(tagRepository.searchMatchingTag("%" + tag + "%"));
        }
        return newTagList;
    }

    @Override
    public List<Tag> fetchTagsByTagNames(List<String> tagNames) {
        List<Tag> tagList = new ArrayList<Tag>();
        if (tagNames == null || tagNames.isEmpty()) {
            return tagList;
        }
        for (String name : tagNames) {
            tagList.add(tagRepository.findDistinctByName(name));
        }
        return tagList;
    }

    @Override
    public List<Tag> saveAllTags(List<Tag> tagList) {
        return tagRepository.saveAll(tagList);
    }

    @Override
    public List<Tag> removePostTags(List<PostTag> postTags) {
        List<Tag> tagList = new ArrayList<Tag>();
        for (PostTag postTag : postTags) {
            postTag.getTags().removePostTag(postTag);
            tagList.add(postTag.getTags());
        }
        return tagList;
    }

    @Override
    public List<Post> fetchAllPostByTag(String search) {
        List<Tag> tagList = tagRepository.getAllTagWithName("%" + search + "%");
        List<Post> postList = new ArrayList<Post>();
        for (Tag tag : tagList) {
            for (PostTag postTag : tag.getPosts()) {
                postList.add(postTag.getPosts());
            }
        }
        return postList;
    }

    @Override
    public List<String> getAllTagNames() {
        return tagRepository.getAllTagNames();
    }

    @Override
    public List<Post> findPostByTagNames(List<String> tags) {
        List<Post> postList = new ArrayList<Post>();
        for (String tagName : tags) {
            Tag tag = tagRepository.findDistinctByName(tagName);
            for (PostTag postTag : tag.getPosts()) {
                postList.add(postTag.getPosts());
            }
        }
        return postList;
    }
}
