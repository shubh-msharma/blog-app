package com.mountblue.blog.service.implementation;

import com.mountblue.blog.entity.Post;
import com.mountblue.blog.entity.User;
import com.mountblue.blog.repository.PostRepository;
import com.mountblue.blog.repository.PostTagRepository;
import com.mountblue.blog.repository.TagRepository;
import com.mountblue.blog.repository.UserRepository;
import com.mountblue.blog.service.interfaces.PostService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Transactional
@Service
public class PostServicesImpl implements PostService {

    @Autowired
    private PostRepository postRepository;
    @Autowired
    private TagRepository tagRepository;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private PostTagRepository postTagRepository;

    @Override
    public List<Post> getAllPost() {
        return postRepository.findAll();
    }

    @Override
    public Post findById(int id) {
        return postRepository.findById(id).orElse(null);
    }

    @Override
    public Page<Post> getPaginatedPost(int page, String sortingOrder) {
        Pageable pageable;
        if (sortingOrder.equals("asc")) {
            pageable = PageRequest.of(page, 4, Sort.by("publishedAt").ascending());
        } else {
            pageable = PageRequest.of(page, 4, Sort.by("publishedAt").descending());
        }
        return postRepository.findAll(pageable);
    }

    @Override
    public Post savePost(Post post) {
        return postRepository.save(post);
    }

    @Override
    public void deletePost(Post post) {
        postRepository.delete(post);
    }

    @Override
    public List<Post> fetchAllPostByAuther(List<User> userList) {
        List<Post> postList = new ArrayList<Post>();
        for (User user : userList) {
            postList.addAll(user.getPosts());
        }
        return postList;
    }

    @Override
    public List<Post> fetchAllPostByTitle(String search) {
        return postRepository.fetchAllPostByTitle("%" + search + "%");
    }

    @Override
    public List<Post> fetchAllPostByContent(String search) {
        return postRepository.fetchAllPostByContent("%" + search + "%");
    }

    @Override
    public List<Post> fetchAllPostByDate(Date date) {
        return postRepository.fetchAllPostByDate(date);
    }

    @Override
    public Date getCurrentDate() throws ParseException {
        Date date = new Date();
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-mm-dd");
         date = simpleDateFormat.parse(simpleDateFormat.format(date));
        return date;
    }

    @Override
    public Date getCurrentDate(String dateStr) throws ParseException {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-mm-dd");
        Date date = simpleDateFormat.parse(dateStr);
        return date;
    }

    @Override
    public Post getTransientPostObject(String content, String title) {
        Post post = new Post();
        post.setTitle(title);
        post.setContent(content);
        if (content.length() < 100) {
            post.setExcerpt(content + ".....");
        } else {
            post.setExcerpt(content.substring(0, 100) + ".....");
        }
        return post;
    }

}