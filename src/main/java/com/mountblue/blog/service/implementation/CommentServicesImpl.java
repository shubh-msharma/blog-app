package com.mountblue.blog.service.implementation;

import com.mountblue.blog.entity.Comment;
import com.mountblue.blog.entity.Post;
import com.mountblue.blog.entity.User;
import com.mountblue.blog.repository.CommentRepository;
import com.mountblue.blog.service.interfaces.CommentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;

@Transactional
@Service
public class CommentServicesImpl implements CommentService {
    @Autowired
    private CommentRepository commentRepository;

    public Comment addComment(String content, Post post, User user) {
        Comment comment = new Comment();
        comment.setComment(content);
        comment.setCreatedAt(new Date());
        comment.setPost(post);
        comment.setUser(user);
        return commentRepository.save(comment);
    }

    @Override
    public Comment getTransientCommentObject(Post post, User user, String commentText) {
        Comment comment = new Comment();
        comment.setPost(post);
        comment.setUser(user);
        comment.setComment(commentText);
        return comment;
    }

    @Override
    public Comment saveComment(Comment comment) {
        return commentRepository.save(comment);
    }

    @Override
    public Comment findCommentByUserAndPost(User user, Post post) {
        return commentRepository.findByUserAndPost(user, post);
    }

    @Override
    public void deleteComment(Comment comment) {
        commentRepository.delete(comment);
    }

    @Override
    public void deleteAllComments(List<Comment> commentList) {
        commentRepository.deleteAll(commentList);
    }

    @Override
    public Comment findById(int commentId) {
        return commentRepository.findById(commentId).orElse(null);
    }
}
