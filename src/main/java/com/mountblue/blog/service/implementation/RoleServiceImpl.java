package com.mountblue.blog.service.implementation;

import com.mountblue.blog.entity.Role;
import com.mountblue.blog.repository.RoleRepository;
import com.mountblue.blog.service.interfaces.RoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class RoleServiceImpl implements RoleService {
    @Autowired
    private RoleRepository roleRepository;

    public Role saveRole(Role role) {
        return roleRepository.save(role);
    }

    @Override
    public Role findRoleByRole(String role) {
        return roleRepository.findByRole(role);
    }
}
