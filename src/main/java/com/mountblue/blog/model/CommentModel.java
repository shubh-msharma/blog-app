package com.mountblue.blog.model;

public class CommentModel {
    private String comment;
    private String name;
    private String mail;

    public CommentModel() {
    }

    public CommentModel(String comment, String name, String mail) {
        this.comment = comment;
        this.name = name;
        this.mail = mail;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    @Override
    public String toString() {
        return "CommentModel{" +
                "comment='" + comment + '\'' +
                ", name='" + name + '\'' +
                ", mail='" + mail + '\'' +
                '}';
    }
}
