package com.mountblue.blog.model;
import java.util.List;
public class FilterParamModel {
    private List<String> tags;
    private List<String> users;
    private String date;

    public FilterParamModel() {
    }

    public FilterParamModel(List<String> tags, List<String> users, String date) {
        this.tags = tags;
        this.users = users;
        this.date = date;
    }

    public List<String> getTags() {
        return tags;
    }

    public void setTags(List<String> tags) {
        this.tags = tags;
    }

    public List<String> getUsers() {
        return users;
    }

    public void setUsers(List<String> users) {
        this.users = users;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    @Override
    public String toString() {
        return "FilterParamModel{" +
                "tags=" + tags +
                ", users=" + users +
                ", date='" + date + '\'' +
                '}';
    }
}
