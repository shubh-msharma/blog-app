package com.mountblue.blog.model;

import java.util.List;

public class PostModel {
    private String name;
    private String mail;
    private String blog;
    private List<String> tags;
    private String title;
    private String publish;

    public PostModel() {
    }

    public PostModel(String name, String mail, String blog, List<String> tags, String title) {
        this.name = name;
        this.mail = mail;
        this.blog = blog;
        this.tags = tags;
        this.title = title;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public String getBlog() {
        return blog;
    }

    public void setBlog(String blog) {
        this.blog = blog;
    }

    public List<String> getTags() {
        return tags;
    }

    public void setTags(List<String> tags) {
        this.tags = tags;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getPublish() {
        return publish;
    }

    public void setPublish(String publish) {
        this.publish = publish;
    }

    @Override
    public String toString() {
        return "PostModel{" +
                "name='" + name + '\'' +
                ", mail='" + mail + '\'' +
                ", blog='" + blog + '\'' +
                ", tags=" + tags +
                ", title='" + title + '\'' +
                ", publish='" + publish + '\'' +
                '}';
    }
}
