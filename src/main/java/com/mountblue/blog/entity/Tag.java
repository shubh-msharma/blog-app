package com.mountblue.blog.entity;


import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Entity
@Table
public class Tag {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    private String name;
    @Column(name = "created_at")
    private Date createdAt = new Date();
    @Column(name = "updated_at")
    private Date updatedAt = new Date();
    @OneToMany(mappedBy = "tags", cascade = CascadeType.REMOVE)
    private List<PostTag> posts = new ArrayList<PostTag>();

    public Tag() {
    }

    public Tag(int id, String name, Date createdAt, Date updatedAt, List<PostTag> posts) {
        this.id = id;
        this.name = name;
        this.createdAt = createdAt;
        this.updatedAt = updatedAt;
        this.posts = posts;
    }

    public Tag(String name) {
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }

    public List<PostTag> getPosts() {
        return new ArrayList<PostTag>(posts);
    }

    public void setPosts(PostTag posts) {
        this.posts.add(posts);
        posts.setTags(this);
    }

    public PostTag removePostTag(PostTag postTag) {
        if (this.posts.remove(postTag)) {
            return postTag;
        } else {
            return null;
        }
    }
}
