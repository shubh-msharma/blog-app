package com.mountblue.blog.entity.compositKey;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;
import java.util.Objects;

@Embeddable
public class postTagId implements Serializable {

    @Column(name = "post_id")
    private int postId;
    @Column(name = "tag_id")
    private int tagId;

    public postTagId() {
    }

    public postTagId(int postId, int tagId) {
        this.postId = postId;
        this.tagId = tagId;
    }

    public int getPostId() {
        return postId;
    }

    public void setPostId(int postId) {
        this.postId = postId;
    }

    public int getTagId() {
        return tagId;
    }

    public void setTagId(int tagId) {
        this.tagId = tagId;
    }

    @Override
    public int hashCode() {
        return Objects.hash(postId,tagId);
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (obj == null || getClass() != obj.getClass()) return false;
        postTagId postTagObj = (postTagId) obj;
        return Objects.equals(this.postId, postTagObj.postId) && Objects.equals(this.tagId, postTagObj.tagId);
    }
}