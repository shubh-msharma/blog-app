package com.mountblue.blog.entity;


import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Entity
@Table
public class Post {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    private String title;
    @Column(length = 2500)
    private String excerpt;
    @Column(length = 5000)
    private String content;
    @ManyToOne
    private User author;
    @Column(name = "published_at")
    private Date publishedAt;
    @Column(name = "is_published")
    private Boolean isPublished;
    @Temporal(TemporalType.DATE)
    @Column(name = "created_at")
    private Date createdAt = new Date();
    @Column(name = "updated_at")
    private Date updatedAt = new Date();
    @OneToMany(mappedBy = "posts", cascade = CascadeType.REMOVE)
    private List<PostTag> tags = new ArrayList<PostTag>();
    @OneToMany(mappedBy = "post")
    private List<Comment> comments = new ArrayList<Comment>();

    public Post(int id, String title, String excerpt, String content, User author, Date publishedAt, Boolean isPublished, Date createdAt, Date updatedAt, List<PostTag> tags) {
        this.id = id;
        this.title = title;
        this.excerpt = excerpt;
        this.content = content;
        this.author = author;
        this.publishedAt = publishedAt;
        this.isPublished = isPublished;
        this.createdAt = createdAt;
        this.updatedAt = updatedAt;
        this.tags = tags;
    }

    public Post() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getExcerpt() {
        return excerpt;
    }

    public void setExcerpt(String excerpt) {
        this.excerpt = excerpt;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public User getAuthor() {
        return author;
    }

    public void setAuthor(User author) {
        this.author = author;
    }

    public Date getPublishedAt() {
        return publishedAt;
    }

    public void setPublishedAt(Date publishedAt) {
        this.publishedAt = publishedAt;
    }

    public Boolean getPublished() {
        return this.isPublished;
    }

    public void setPublished(Boolean published) {
        this.isPublished = published;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }

    public List<PostTag> getTags() {
        return new ArrayList<PostTag>(tags);
    }

    public void setTags(PostTag tags) {
        this.tags.add(tags);
        tags.setPosts(this);
    }

    public List<Comment> getComments() {
        return new ArrayList<Comment>(comments);
    }

    public void setComments(Comment comment) {
        this.comments.add(comment);
        comment.setPost(this);
    }

    public Comment removeComment(Comment comment) {
        if (this.comments.contains(comment)) {
            if (this.comments.remove(comment)) {
                return comment;
            } else {
                return null;
            }
        } else {
            return null;
        }
    }

    public PostTag removePostTag(PostTag postTag) {
        if (this.tags.remove(postTag)) {
            return postTag;
        } else {
            return null;
        }
    }

    @Override
    public String toString() {
        return "Post{" +
                "id=" + id +
                ", title='" + title + '\'' +
                ", excerpt='" + excerpt + '\'' +
                ", content='" + content + '\'' +
                ", author=" + author +
                ", publishedAt=" + publishedAt +
                ", isPublished=" + isPublished +
                ", createdAt=" + createdAt +
                ", updatedAt=" + updatedAt +
                ", tags=" + tags +
                '}';
    }
}
