package com.mountblue.blog.entity;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@Entity
@Table(name = "users", uniqueConstraints = @UniqueConstraint(columnNames = "email"))
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    private String name;
    private String email;
    private String password;
    @OneToMany(mappedBy = "author", cascade = CascadeType.REMOVE)
    private List<Post> post = new ArrayList<Post>();
    @OneToMany(mappedBy = "user", cascade = CascadeType.REMOVE)
    private List<Comment> comments = new ArrayList<Comment>();
    @ManyToMany(fetch = FetchType.EAGER, mappedBy = "users")
    private Collection<Role> roles = new ArrayList<Role>();

    public User(int id, String name, String email, String password, List<Post> posts, List<Comment> comments) {
        this.id = id;
        this.name = name;
        this.email = email;
        this.password = password;
        this.post = posts;
        this.comments = comments;
    }

    public User() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public List<Post> getPosts() {
        return new ArrayList<Post>(post);
    }

    public void setPosts(Post posts) {
        this.post.add(posts);
        posts.setAuthor(this);
    }

    public Post removePost(Post post) {
        if (this.getPosts().contains(post)) {
            if (this.getPosts().remove(post)) {
                return post;
            } else {
                return null;
            }
        } else {
            return null;
        }
    }

    public List<Comment> getComments() {
        return new ArrayList<Comment>(comments);
    }

    public void setComments(Comment comment) {
        this.comments.add(comment);
        comment.setUser(this);
    }

    public Comment removeComment(Comment comment) {
        if (this.comments.contains(comment)) {
            if (this.comments.remove(comment)) {
                return comment;
            } else {
                return null;
            }
        } else {
            return null;
        }
    }

    public Collection<Role> getRoles() {
        return new ArrayList<Role>(this.roles);
    }

    public void setRoles(Role role) {
        this.roles.add(role);
    }

}
