package com.mountblue.blog.entity;


import com.mountblue.blog.entity.compositKey.postTagId;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.Objects;

@Entity
@Table
public class PostTag implements Serializable {
    @Id
    private postTagId id;
    @ManyToOne
    @MapsId("tagId")
    private Tag tags;
    @ManyToOne
    @MapsId("postId")
    private Post posts;
    @Column(name = "created_at")
    private Date createdAt = new Date();
    @Column(name = "updated_at")
    private Date updatedAt = new Date();

    public PostTag(postTagId id, Tag tags, Post posts, Date createdAt, Date updatedAt) {
        this.id = id;
        this.tags = tags;
        this.posts = posts;
    }

    public PostTag() {
    }

    public postTagId getId() {
        return id;
    }

    public void setId(postTagId id) {
        this.id = id;
    }

    public Tag getTags() {
        return tags;
    }

    public void setTags(Tag tags) {
        this.tags = tags;
    }

    public Post getPosts() {
        return posts;
    }

    public void setPosts(Post posts) {
        this.posts = posts;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }

    @Override
    public int hashCode() {
        return Objects.hash(posts, tags);
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;

        if (obj == null || getClass() != obj.getClass())
            return false;

        PostTag postTagObj = (PostTag) obj;
        return Objects.equals(posts, postTagObj.posts) &&
                Objects.equals(tags, postTagObj.tags);
    }

    @Override
    public String toString() {
        return "PostTag{" +
                "id=" + id +
                ", tags=" + tags +
                ", createdAt=" + createdAt +
                ", updatedAt=" + updatedAt +
                '}';
    }
}
