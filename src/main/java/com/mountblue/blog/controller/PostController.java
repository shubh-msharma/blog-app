package com.mountblue.blog.controller;


import com.mountblue.blog.entity.*;
import com.mountblue.blog.model.FilterParamModel;
import com.mountblue.blog.model.PostModel;
import com.mountblue.blog.service.interfaces.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.view.RedirectView;

import java.text.ParseException;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Controller
public class PostController {
    private static final int firstElement = 0;
    private static final int secondElement = 1;
    @Autowired
    private PostService postService;
    @Autowired
    private UserService userService;
    @Autowired
    private TagService tagService;
    @Autowired
    private PostTagService postTagService;
    @Autowired
    private CommentService commentService;

    @RequestMapping("/addpost")
    public String addPost(Model model) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        User user = userService.findUserByEmail(auth.getName());
        model.addAttribute("user", user);
        model.addAttribute("admin", userService.isAdmin());
        model.addAttribute("name", SecurityContextHolder.getContext().getAuthentication().getName());
        return "createBlog";
    }

    @RequestMapping("/getallposts/{page}")
    public String getAllPosts(Model model, @PathVariable("page") int page, @RequestParam("sortby") String sortingOrder) {
        Page<Post> pageOfPost = postService.getPaginatedPost(page, sortingOrder);
        List<String> tagNames = tagService.getAllTagNames();
        List<String> userNames = userService.getAllUserNames();
        model.addAttribute("totalPages", pageOfPost.getTotalPages());
        model.addAttribute("totalElements", pageOfPost.getTotalElements());
        model.addAttribute("number", pageOfPost.getNumber());
        model.addAttribute("sortby", sortingOrder);
        model.addAttribute("tags", tagNames);
        model.addAttribute("users", userNames);
        List<Post> posts = pageOfPost.getContent();
        model.addAttribute("posts", posts);
        model.addAttribute("name", SecurityContextHolder.getContext().getAuthentication().getName());
        return "showAllPost";
    }

    @RequestMapping(value = "/savePost", method = RequestMethod.POST)
    public String savePost(@ModelAttribute("newPost") PostModel newPost, Model model) throws ParseException {
        User user = userService.findUserByEmail(newPost.getMail());
        if (user == null) {
            user = new User();
            user.setEmail(newPost.getMail());
            user.setName(newPost.getName());
        }
        Post post = postService.getTransientPostObject(newPost.getBlog(), newPost.getTitle());
        post.setCreatedAt(postService.getCurrentDate());
        post.setUpdatedAt(postService.getCurrentDate());
        if (newPost.getPublish() != null) {
            post.setPublished(true);
            post.setPublishedAt(postService.getCurrentDate());
        } else {
            post.setPublished(false);
        }
        List<Tag> tagList = tagService.fetchTagsByTagNames(newPost.getTags());
        List<PostTag> postTagsList = postTagService.fetchPostTagsByTagAndPost(tagList, post);
        user.setPosts(post);
        post.setAuthor(user);
        user = userService.saveUser(user);
        post = postService.savePost(post);
        tagService.saveAllTags(tagList);
        postTagService.saveAllPostTags(postTagsList);
        model.addAttribute("post", post);
        model.addAttribute("name", SecurityContextHolder.getContext().getAuthentication().getName());
        model.addAttribute("published", post.getPublished());
        model.addAttribute("comments", post.getComments());
        model.addAttribute("show", userService.isAllowedToModify(post));
        return "showOnePost";
    }

    @RequestMapping("/showPost/{id}")
    public String showPost(@PathVariable("id") String id, Model model) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        int convertedId = Integer.parseInt(id);
        Post post = postService.findById(convertedId);
        if (post.getPublished() == null) {
            model.addAttribute("published", false);
            return "showOnePost";
        } else if (post.getPublished() == false && auth.getName().equals("anonymousUser")) {
            model.addAttribute("published", false);
            return "showOnePost";
        } else if (post.getPublished() == true && auth.getName().equals("anonymousUser")) {
            model.addAttribute("post", post);
            model.addAttribute("comments", post.getComments());
            model.addAttribute("published", true);
            model.addAttribute("show", false);
            return "showOnePost";
        } else if (post.getPublished() == false && !auth.getName().equals("anonymousUser")) {
            boolean isAllowed = userService.isAllowedToModify(post);
            if (isAllowed) {
                model.addAttribute("published", true);
                model.addAttribute("show", isAllowed);
                model.addAttribute("post", post);
                model.addAttribute("comments", post.getComments());
            } else {
                model.addAttribute("published", false);
            }
        } else if (post.getPublished() == true && !auth.getName().equals("anonymousUser")) {
            model.addAttribute("post", post);
            model.addAttribute("comments", post.getComments());
            model.addAttribute("published", true);
            boolean isAllowed = userService.isAllowedToModify(post);
            if (isAllowed) {
                model.addAttribute("show", isAllowed);
            } else {
                model.addAttribute("show", false);
            }
        }
        model.addAttribute("name", auth.getName());
        return "showOnePost";
    }

    @RequestMapping("/editPost/{id}")
    public String editPost(@PathVariable("id") int id, Model model) {
        model.addAttribute("post", postService.findById(id));
        model.addAttribute("admin", userService.isAdmin());
        return "createBlog";
    }

    @RequestMapping("/deletePost/{id}")
    public RedirectView deletePost(@PathVariable("id") int id) {
        Post post = postService.findById(id);
        User user = post.getAuthor();
        List<PostTag> postTags = post.getTags();
        List<Comment> commentList = post.getComments();
        userService.removeCommentsFromUser(user, commentList);
        List<Tag> tags = tagService.removePostTags(postTags);
        post.getTags().clear();
        user.removePost(post);
        commentService.deleteAllComments(commentList);
        postTagService.deleteAll(postTags);
        tagService.saveAllTags(tags);
        userService.saveUser(user);
        postService.deletePost(post);
        RedirectView rv = new RedirectView();
        rv.setUrl("/getallposts/0/?sortby=");
        return rv;
    }

    @RequestMapping(value = "/updatePost/{id}", method = RequestMethod.POST)
    public String updatePost(@ModelAttribute("newPost") PostModel newPost, Model model, @PathVariable("id") int id) throws ParseException {
        Post oldPost = postService.findById(id);
        List<Tag> tagList = tagService.fetchTagsByTagNames(newPost.getTags());
        List[] updationList = postTagService.fetchPostTagAndTagByPostAndTag(tagList, oldPost);
        List<PostTag> postTagsToBeRemoved = updationList[firstElement];
        List<Tag> tagToBeUpdated = updationList[secondElement];
        if (!postTagsToBeRemoved.isEmpty()) postTagService.deleteAll(postTagsToBeRemoved);
        if (!tagToBeUpdated.isEmpty()) tagService.saveAllTags(tagToBeUpdated);
        if (postTagsToBeRemoved.isEmpty() && tagToBeUpdated.isEmpty()) oldPost = postService.savePost(oldPost);
        oldPost.setContent(newPost.getBlog());
        oldPost.setTitle(newPost.getTitle());
        oldPost.setUpdatedAt(postService.getCurrentDate());
        if (newPost.getPublish() != null) {
            oldPost.setPublished(true);
            oldPost.setUpdatedAt(postService.getCurrentDate());
        } else {
            oldPost.setPublished(false);
            oldPost.setPublishedAt(null);
        }
        List<PostTag> postTagsList = postTagService.fetchPostTagsByTagAndPost(tagList, oldPost);
        tagService.saveAllTags(tagList);
        postTagService.saveAllPostTags(postTagsList);
        Post updatedPost = postService.savePost(oldPost);
        model.addAttribute("post", updatedPost);
        model.addAttribute("published", updatedPost.getPublished());
        model.addAttribute("comments", updatedPost.getComments());
        model.addAttribute("show", userService.isAllowedToModify(updatedPost));
        model.addAttribute("name", SecurityContextHolder.getContext().getAuthentication().getName());
        return "showOnePost";
    }

    @RequestMapping("/publishPost/{id}")
    public RedirectView publishPost(@PathVariable("id") int id) throws ParseException {
        Post post = postService.findById(id);
        post.setPublished(true);
        post.setPublishedAt(postService.getCurrentDate());
        postService.savePost(post);
        RedirectView rv = new RedirectView();
        rv.setUrl("/showPost/" + id);
        return rv;
    }

    @RequestMapping("/search")
    public String seachPost(@RequestParam("search") String search, Model model) {
        Set<Post> postSet = new HashSet<Post>();
        List<User> userList = userService.findAllUserWithName(search);
        postSet.addAll(postService.fetchAllPostByAuther(userList));
        postSet.addAll(postService.fetchAllPostByTitle(search));
        postSet.addAll(postService.fetchAllPostByContent(search));
        postSet.addAll(tagService.fetchAllPostByTag(search));
        model.addAttribute("posts", postSet);
        model.addAttribute("number", 0);
        model.addAttribute("searchResult", true);
        model.addAttribute("name", SecurityContextHolder.getContext().getAuthentication().getName());
        return "showAllPost";
    }

    @RequestMapping(value = "/filter", method = RequestMethod.POST)
    public String filterPost(@ModelAttribute("filters") FilterParamModel filters, Model model) throws ParseException {
        Set<Post> postList = new HashSet<Post>();
        Date newDate = null;
        if (filters.getUsers() != null) {
            postList.addAll(userService.findPostByUserNames(filters.getUsers()));
            if (filters.getTags() != null) {
                postList = postList.stream().filter(post -> post.getTags().stream().anyMatch(postTag -> filters.getTags().contains(postTag.getTags().getName()))).collect(Collectors.toSet());
            }
            if (!filters.getDate().isEmpty()) {
                postList.addAll(postService.fetchAllPostByDate(postService.getCurrentDate(filters.getDate())));
            }
        } else if (filters.getTags() != null) {
            postList.addAll(tagService.findPostByTagNames(filters.getTags()));
            if (!filters.getDate().isEmpty()) {
                postList.addAll(postService.fetchAllPostByDate(postService.getCurrentDate(filters.getDate())));
            }
        } else {
            if (!filters.getDate().isEmpty()) {
                postList.addAll(postService.fetchAllPostByDate(postService.getCurrentDate(filters.getDate())));
            }
        }
        model.addAttribute("tags", tagService.getAllTagNames());
        model.addAttribute("users", userService.getAllUserNames());
        model.addAttribute("posts", postList);
        model.addAttribute("number", 0);
        model.addAttribute("searchResult", true);
        model.addAttribute("name", SecurityContextHolder.getContext().getAuthentication().getName());
        return "showAllPost";
    }
}

