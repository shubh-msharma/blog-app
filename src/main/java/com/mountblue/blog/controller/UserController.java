package com.mountblue.blog.controller;

import com.mountblue.blog.entity.Role;
import com.mountblue.blog.entity.User;
import com.mountblue.blog.model.UserModel;
import com.mountblue.blog.service.interfaces.RoleService;
import com.mountblue.blog.service.interfaces.TagService;
import com.mountblue.blog.service.interfaces.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class UserController {
    @Autowired
    private UserService userService;
    @Autowired
    private RoleService roleService;
    @Autowired
    private TagService tagService;
    @Autowired
    private PasswordEncoder passwordEncoder;

    @RequestMapping("/")
    public String redirectToHome(Model model) {
        model.addAttribute("user", new UserModel());
        return "redirect:/home";
    }

    @RequestMapping("/home")
    public String showHome(Model model) {
        model.addAttribute("user", new UserModel());
        model.addAttribute("name", SecurityContextHolder.getContext().getAuthentication().getName());
        return "home";
    }

    @RequestMapping("/login")
    public String login(Model model) {
        return "login";
    }

    @RequestMapping("/registerUser")
    public String registerUser(@ModelAttribute("user") UserModel user) {
        User newUser = new User();
        if (user.getRole() == null) {
            user.setRole("auther");
        }
        Role role = roleService.findRoleByRole(user.getRole());
        if (role == null) {
            role = new Role();
            role.setRole(user.getRole());
        }
        newUser.setEmail(user.getEmail());
        newUser.setName(user.getName());
        newUser.setPassword(passwordEncoder.encode(user.getPassword()));
        role.setUsers(newUser);
        newUser.setRoles(role);
        newUser = userService.saveUser(newUser);
        roleService.saveRole(role);
        return "redirect:/home?success";
    }

    @RequestMapping("/profile")
    public String getProfile(Model model) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        if (auth.getName() != "anonymousUser") {
            User user = userService.findUserByEmail(auth.getName());
            model.addAttribute("sortby", "acs");
            model.addAttribute("tags", tagService.getAllTagNames());
            model.addAttribute("users", userService.getAllUserNames());
            model.addAttribute("posts", user.getPosts());
            model.addAttribute("searchResult", true);
            model.addAttribute("name", SecurityContextHolder.getContext().getAuthentication().getName());
        }
        return "showAllPost";
    }
}
