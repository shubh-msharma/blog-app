package com.mountblue.blog.controller;


import com.mountblue.blog.entity.Comment;
import com.mountblue.blog.entity.Post;
import com.mountblue.blog.entity.User;
import com.mountblue.blog.model.CommentModel;
import com.mountblue.blog.service.interfaces.CommentService;
import com.mountblue.blog.service.interfaces.PostService;
import com.mountblue.blog.service.interfaces.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.view.RedirectView;

@Controller
public class CommentController {
    @Autowired
    PostService postService;
    @Autowired
    UserService userService;
    @Autowired
    CommentService commentService;

    @PostMapping("/addComment/{id}")
    public RedirectView addComment(@ModelAttribute("comment") CommentModel comment, @PathVariable("id") int id, Model model) {
        Post post = postService.findById(id);
        User user = userService.findUserByEmail(comment.getMail());
        if (user == null) {
            user = userService.getTransientUserObject(comment.getName(), comment.getMail());
        }
        Comment newComment = commentService.getTransientCommentObject(post, user, comment.getComment());
        user.setComments(newComment);
        post.setComments(newComment);
        userService.saveUser(user);
        postService.savePost(post);
        commentService.saveComment(newComment);
        RedirectView rv = new RedirectView();
        rv.setUrl("/showPost/" + id);
        return rv;
    }

    @RequestMapping("/deleteComment/{commentId}/{postId}")
    public RedirectView removeComment(@PathVariable("commentId") int commentId, @PathVariable("postId") int postId) {
        Comment comment = commentService.findById(commentId);
        Post post = postService.findById(postId);
        User user = comment.getUser();
        post.removeComment(comment);
        user.removeComment(comment);
        postService.savePost(post);
        userService.saveUser(user);
        commentService.deleteComment(comment);
        RedirectView rv = new RedirectView();
        rv.setUrl("/showPost/" + post.getId());
        return rv;
    }

    @RequestMapping("/editComment/{commentId}/{postId}")
    public String editComment(@PathVariable("commentId") int commentId, @PathVariable("postId") int postId, Model model) {
        Comment comment = commentService.findById(commentId);
        Post post = postService.findById(postId);
        User user = comment.getUser();
        post.removeComment(comment);
        user.removeComment(comment);
        postService.savePost(post);
        userService.saveUser(user);
        commentService.deleteComment(comment);
        CommentModel commentModel = new CommentModel();
        commentModel.setComment(comment.getComment());
        commentModel.setMail(user.getEmail());
        commentModel.setName(user.getName());
        model.addAttribute("editComment", commentModel);
        model.addAttribute("post", post);
        model.addAttribute("name", SecurityContextHolder.getContext().getAuthentication().getName());
        model.addAttribute("published", post.getPublished());
        model.addAttribute("comments", post.getComments());
        model.addAttribute("show", userService.isAllowedToModify(post));
        return "showOnePost";
    }

}
