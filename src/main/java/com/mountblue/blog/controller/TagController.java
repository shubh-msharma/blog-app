package com.mountblue.blog.controller;
import com.mountblue.blog.entity.Tag;
import com.mountblue.blog.service.implementation.TagServicesImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import java.util.List;
@RestController
public class TagController {
    @Autowired
    private TagServicesImpl tagServices;
    @GetMapping("/addNewTag/{tags}")
    public Tag addNewTag(@PathVariable("tags") String tag) {
        return tagServices.saveTag(tag);
    }

    @PostMapping("/getRelatedTags")
    public List<String> getRelatedTag(@RequestBody List<String> tagList) {
        return tagServices.searchMatchingTag(tagList);
    }

    @PostMapping("/getAllTagNames")
    public List<String> getAllTagNames(){
        return tagServices.getAllTagNames();
    }
}
