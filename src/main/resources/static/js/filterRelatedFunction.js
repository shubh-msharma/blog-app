const form = document.querySelector("#changer");
form.addEventListener('change',(e)=>{
    e.preventDefault();
    const filterby = form.filterby.value;

    if(filterby === "tag"){
        const form = createElements("form",null,null,"submission",null,null,"/filterbyTag");
        const select = createElements("select","form-control",null,null,null,null,null,"tag");
        const eleDiv = createElements("div","form-group");

        fetch("/getAllTagNames",{method:"POST"})
                    .then(res=>res.json())
                    .then(res=>{
                        res.forEach((ele,i)=>{

                                        if(i==0){
                                            select.append(createElements(
                                                "option",
                                                null,null,null,null,null,null,null,"filter by tag"
                                            ))
                                        }
                                        select.append(createElements(
                                            "option",
                                            null,null,null,ele,null,null,null,ele
                                        ))
                                    })
                                    eleDiv.append(select);
                                    form.append(eleDiv);
                                    document.getElementById("box").innerHTML = ""
                                    document.getElementById("box").append(form);
                                    select.addEventListener('change',function(){
                                        form.submit();
                                    })
                    })
    }

    if(filterby === "auther"){
        const form = createElements("form",null,null,"submission",null,null,"/filterbyAuther");
        const select = createElements("select","form-control",null,null,null,null,null,"auther");
        const eleDiv = createElements("div","form-group");

        fetch("/getAllUserNames",{method:"POST"})
            .then(res=>res.json())
            .then(res=>{
                res.forEach((ele,i)=>{
                                if(i==0){
                                    select.append(createElements(
                                        "option",
                                        null,null,null,null,null,null,null,"filter by auther"
                                    ))
                                }
                                select.append(createElements(
                                    "option",
                                    null,null,null,ele,null,null,null,ele
                                ))
                            })
                            eleDiv.append(select);
                            form.append(eleDiv);
                            document.getElementById("box").innerHTML = ""
                            document.getElementById("box").append(form);
                            select.addEventListener('change',function(){
                                form.submit();
                            })
            })
    }

    if(filterby === "date"){
        const form = createElements("form",null,null,"submission",null,null,"/filterbyDate");
        const dateInput = createElements("input","form-control","date",null,null,null,null,"date");
        const eleDiv = createElements("div","form-group");
        eleDiv.append(dateInput);
        form.append(eleDiv);
        document.getElementById("box").innerHTML = ""
        document.getElementById("box").append(form);
        dateInput.addEventListener('change',function(){
            form.submit();
        })
    }

})

function createElements(elem, css, type = null, id = null, value = null, for_ = null,action=null,name=null,innertext = null) {

    const ele = document.createElement(elem);
    ele.className = css;
    if (type)
        ele.type = type
    if (id)
        ele.id = id;
    if (value)
        ele.value = value;
    if (for_)
        ele.for = for_;
    if (innertext)
        ele.textContent = innertext;
    if(action)
        ele.action = action;
    if(name)
        ele.name = name;
    if(innertext)
        ele.innertext =  innertext
    return ele;
}