const id = document.getElementById("post123").textContent;
window.addEventListener('load',(e)=>{
fetch("/getComment/"+id).then(res=>{
    console.log(res);
        return res.json();
    }).then(res=>{
    console.log(res);
    if(res.length == 0) return;
    res.forEach(comments=>{
    console.log(comments);
        const mainDiv = createElements("div",
                "flex;flex-direction: column; border-bottom: 1px solid black; padding: 5px; margin-bottom: 2px;",
                null,null,null,null,null,"parent");
                const userDiv = createElements("div",
                "display: flex;flex-direction: row;justify-content: space-between;")
                const userName = createElements("h6"," ",null,"userName",null,null,comments.name);
                const userMail = createElements("h6"," ",null,"userMail",null,null,comments.mail);
                const comment = createElements("p"," ",null,"comment",null,null,comments.comment);
                userDiv.append(userName);
                userDiv.append(userMail);
                mainDiv.append(userDiv);
                mainDiv.append(comment);
                document.getElementById("commentContainer").append(mainDiv);
    })

    })

})

const form = document.querySelector("#commentForm");

form.addEventListener('submit',(e)=>{
    e.preventDefault();
    const name = form.name.value;
    const mail = form.mail.value;
    const comment = form.comment.value;
    const postId = form.postId.value
    const data = JSON.stringify({
        name,mail,comment,postId
    })
    console.log(data);
    fetch("/addComment",{
        method:"POST",
         headers: {
                    'Content-Type': 'application/json'
                },
        body:data
    }).then(res=>{
        return res.json();
    }).then(res=>{
        const mainDiv = createElements("div",
        "flex;flex-direction: column; border-bottom: 1px solid black; padding: 5px; margin-bottom: 2px;",
        null,null,null,null,null,"parent");
        const userDiv = createElements("div",
        "display: flex;flex-direction: row;justify-content: space-between;")
        const userName = createElements("h6"," ",null,"userName",null,null,res.name);
        const userMail = createElements("h6"," ",null,"userMail",null,null,res.mail);
        const comment = createElements("p"," ",null,"comment",null,null,res.comment);
        userDiv.append(userName);
        userDiv.append(userMail);
        mainDiv.append(userDiv);
        mainDiv.append(comment);
        document.getElementById("commentContainer").append(mainDiv);
    })
})
function createElements(elem, css, type = null, id = null, value = null, for_ = null, innertext = null,cssClass=null) {

    const ele = document.createElement(elem);
    ele.style.cssText = css;
    if (type)
        ele.type = type
    if (id)
        ele.id = id;
    if (value)
        ele.value = value;
    if (for_)
        ele.for = for_;
    if (innertext)
        ele.textContent = innertext;
     if(cssClass){
        ele.className = cssClass;
     }
    return ele;
}