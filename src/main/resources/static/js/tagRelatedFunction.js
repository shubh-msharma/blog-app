const state = {
    previousVal:""
}

document.querySelector("#addTag").addEventListener("click", (e) => {
    const val = e.target.parentElement.nextElementSibling.value;
    console.log(val);
    if (val === null) return
    if (val.length === 0) return

    fetch("/addNewTag/"+val).then(res => {
        if (res.status === 200) {
            const div = createElements('div', "form-check form-check-inline");
            const input = createElements("input", "form-check-input", "checkbox", val, val, null, null);
            input.name = "tags"
            input.checked =true;
            const label = createElements("label", "form-check-label", null, null, null, val, val);
            div.append(input);
            div.append(label);
            document.querySelector("#tags").append(div);
        }
    })

    e.target.parentElement.nextElementSibling.value = "";

})

document.querySelector("#title").addEventListener("input",(e)=>{
if(! document.querySelector("#title").hasEventListner){
    document.querySelector("#title").hasEventListner = true;
    document.querySelector("#title").addEventListener("blur",searchTags)
}
})

function searchTags(e){
const val = e.target.value;
    if(val === state.previousVal)return;
    state.previousVal = val;
    if(val===null)return;
    if(val.length === 0)return;
    const strs = JSON.stringify(val.split(" ").filter(str=>str.length>=4));
    console.log(strs)
    fetch("/getRelatedTags",{
        method:"POST",
         headers: {
                    'Content-Type': 'application/json'
                },
        body:strs
    }).then(res=>{
        return res.json()
    }).then(res=>{
        res.forEach(tag=>{
        const div = createElements('div', "form-check form-check-inline");
                    const input = createElements("input", "form-check-input", "checkbox", tag, tag, null, null);
                    const label = createElements("label", "form-check-label", null, null, null, tag, tag);
                    input.name = "tags"
                    div.append(input);
                    div.append(label);
                    document.querySelector("#tags").append(div);
        })
        document.querySelector("#title").hasEventListner = false;
        document.querySelector("#title").removeEventListener('blur',searchTags);

    })
}

function createElements(elem, css, type = null, id = null, value = null, for_ = null, innertext = null) {

    const ele = document.createElement(elem);
    ele.className = css;
    if (type)
        ele.type = type
    if (id)
        ele.id = id;
    if (value)
        ele.value = value;
    if (for_)
        ele.for = for_;
    if (innertext)
        ele.textContent = innertext;
    return ele;
}